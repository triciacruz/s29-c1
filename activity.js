db.post.insertMany([
	{
		title: "Hello World",
		content: "This is my first blog",
		likes: 5,
	},
	
	{
		title: "Hello Guyz",
		content: "Hello everyone, welcome to my blog!",
		likes: 10,
	},

	{
		title: "My firsts posts",
		content: "What's up everyone",
		likes: 0,
	},
]);


db.posts.insertOne({
	title: "Test",
	content: "This is only a test"
});


db.posts.updateOne(
	{title: "Test"},
		{
			$set:{
				title: "This is my second posts",
				content: "Hello friends let's chat!",
				likes: 0,
			}
		}
);

db.posts.deleteMany({likes:0});


// movie_db
db.movies.insertMany([
	{
		title: "The Godfather",
		year:"1972",
		director: "Francis Ford Coppola",
		genre:["Crime","Drama"]
	},
	
	{
		title: "Titanic",
		year:"1997",
		director: "James Cameron",
		genre:["Drama","Romance"]
	},

	{
		title: "The Lion King",
		year:"1994",
		director: "Roger Allers",
		genre:["Animation", "Adventure","Drama"]
	},

	{
		title: "Raiders of the Lost Ark",
		year:"1981",
		director: "Steven Spielberg",
		genre:["Action","Adventure"]
	},

	{
		title: "The Dark Knight",
		year:"2008",
		director: "Christopher Nolan",
		genre:["Action", "Crime","Drama"]
	},
]);


db.movies.find(
	{},
	{
		title:1,
		year:1,
		_id: 0
	}
);


db.movies.updateOne(
	{title: "The Godfather"},
		{
			$set:{
				year: "1971"
			}
		}
);


db.movies.find({
	$and:[
		{genre: "Drama"},
		{year: 
			{$lte: "1990"}
		}
	]
});